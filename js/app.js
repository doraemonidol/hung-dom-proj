let sections = [];
sections = document.querySelectorAll('section');
sections.forEach(function(s){
    let navbarItem = document.createElement('li');
    let aChild = document.createElement('a');

    aChild.innerHTML = s.querySelector('h2').innerHTML;
    aChild.setAttribute('href', '#'+s.getAttribute('id'));
    aChild.classList.add('menu__link');

    navbarItem.appendChild(aChild);

    document.getElementById('navbar__list').appendChild(navbarItem);
});

document.getElementById('navbar__list').style.justifyContent = 'center';

const sectionTwo = document.getElementById('section2');

window.addEventListener('scroll', () => {
    const bounding2 = sectionTwo.getBoundingClientRect();
    // Check if the user has scrolled near section two
    if (bounding2.top < 500) {
        document.getElementById('myBtn').classList.add('on');
    } else {
        document.getElementById('myBtn').classList.remove('on');
    }

    let cur = 0;
    let dist = 50000;
    let index = 0;
    sections.forEach(function(s) {
        const bounding = s.getBoundingClientRect();
        const top = Math.abs(bounding.top - 150);
        //console.log(top);
        if (top < dist) {
            dist = top;
            cur = index;
        }
        index += 1;
    });
    //console.log(current.getAttribute('id'));
    const prev = document.getElementsByClassName('your-active-class');
    //console.log(prev);
    if (prev.length > 0 && prev[0].getAttribute('id') === sections[cur].getAttribute('id')) {

    } else {
        if (prev.length > 0) {
            prev[0].classList.remove('your-active-class');
            document.querySelectorAll('.current')[0].classList.remove('current');
        }
        sections[cur].classList.add('your-active-class');
        document.querySelectorAll('.menu__link')[cur].classList.add('current');
        
    }
});